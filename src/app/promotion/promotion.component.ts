import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-promotion',
  templateUrl: './promotion.component.html',
  styleUrls: ['./promotion.component.scss']
})
export class PromotionComponent implements OnInit {

  constructor(private appService: AppService) { }
  recomendations = [];

  ngOnInit() {
    this.listRecomendations();
  }

  config: SwiperOptions = {
    autoplay: 0, // Autoplay option having value in milliseconds
    initialSlide: 0, // Slide Index Starting from 0
    slidesPerView: 1, // Slides Visible in Single View Default is 1
    // pagination: '.swiper-pagination', // Pagination Class defined
    paginationClickable: true, // Making pagination dots clicable
    nextButton: '.swiper-button-next', // Class for next button
    prevButton: '.swiper-button-prev', // Class for prev button
    spaceBetween: 30 // Space between each Item
  };

  async listRecomendations() {

    const recomendedProducts = await this.appService.getRecomendations('5049');

    recomendedProducts.forEach(async product => {

      const sku = product.id;
      const recomendedProdut = await this.appService.getRecomendedProduct(sku);
      console.log('produtos', recomendedProdut)
      this.recomendations.push(recomendedProdut);

    });

    // this.showLoadRecomendations = false;
  }

}
